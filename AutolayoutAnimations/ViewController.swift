//
//  ViewController.swift
//  AutolayoutAnimations
//
//  Created by James Cash on 02-08-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var dumbView: UIView!

    @IBOutlet weak var centerXConstraint: NSLayoutConstraint!
    @IBOutlet var centerYConstranit: NSLayoutConstraint!

    var topLayoutConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        topLayoutConstraint = dumbView.topAnchor.constraintEqualToAnchor(view.topAnchor)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        UIView.animateWithDuration(3, delay: 0, options: [.Repeat, .Autoreverse], animations: {
//            self.centerYConstranit.constant = 100
            self.centerYConstranit.active = false
            self.topLayoutConstraint.active = true
            self.centerXConstraint.constant = 100
            self.dumbView.backgroundColor = UIColor.cyanColor()
            self.view.layoutIfNeeded()
            }, completion: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

